<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="http://www.google.com/uds/solutions/dynamicfeed/gfdynamicfeedcontrol.js"
type="text/javascript"></script>
<script src="<?php echo base_url();?>design/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>design/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>design/js/clean-blog.min.js"></script>

<style type="text/css">
@import url("http://www.google.com/uds/solutions/dynamicfeed/gfdynamicfeedcontrol.css");

#feedControl {
margin-top : 10px;
margin-left: auto;
margin-right: auto;
width : 440px;
font-size: 12px;
color: #9CADD0;
}

#feedControl1 {
margin-top : 10px;
margin-left: auto;
margin-right: auto;
width : 440px;
font-size: 12px;
color: #9CADD0;
float:bottom;
}
.fa.fa-times.fa-2x
{
        position: absolute;
    right: 47px;
    top: 39px;

}
       
         

</style>
<script type="text/javascript">
function load() {
var feed =" 	http://feeds.bbci.co.uk/news/technology/rss.xml";
new GFdynamicFeedControl(feed, "feedControl");

}
google.load("feeds", "1");
google.setOnLoadCallback(load);

function load1() {
var feed ="http://rss.nytimes.com/services/xml/rss/nyt/Technology.xml";
new GFdynamicFeedControl(feed, "feedControl1");
}
google.load("feeds", "1");
google.setOnLoadCallback(load1);

function load2() {
var feed ="http://timesofindia.feedsportal.com/c/33039/f/533923/index.rss";
new GFdynamicFeedControl(feed, "feedControl2");
}
google.load("feeds", "1");
google.setOnLoadCallback(load2);

function load3() {
var feed ="http://rss.cnn.com/rss/edition_technology.rss";
new GFdynamicFeedControl(feed, "feedControl3");
}
google.load("feeds", "1");
google.setOnLoadCallback(load3);
</script>

    <title>TechBytes</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>design/css/bootstrapraj.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>design/css/clean-blograj.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>design/css/hoverraj.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://www.derbywire.com//assets/css/signin.css' rel='stylesheet' type='text/css' />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<?php   $success_login = $this->session->userdata('success_login');
if($success_login != ''){?>

<script>
$(document).ready(function () {

    $('#myModal').modal('show');

});
</script>
      
<?php }

?>
<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
       
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="ourindex.html" ><img src="<?php echo base_url();?>design/images/hpe.png" style="max-width:60%;cursor:pointer;" class="img-responsive"/></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url();?>welcome/index">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>welcome/register">Register</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>welcome/sheduler">Sheduler</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>welcome/logout">Logout</a>
                    </li>
                    <?php $username=$this->session->userdata('firstname');?>
                    <li>
                        <a href="<?php echo base_url();?>welcome/dashboard">Hi <?php echo $username;?> !</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <header class="intro-header" style="background-image: url('<?php echo base_url();?>design/images/home3.jpg');">
    <!-- Set your background image for this header on the line below. -->
        <div class="container">
		
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading" >
                        <h1>TechBytes</h1>
                        <hr class="small" >
                        <span class="subheading">The Power of Sharing Knowledge</span>
                    </div>
                </div>
            </div>
			
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
		<center>
		<a href="<?php echo base_url()?>welcome/register">
		<div class="hvr-shrink" style="float:left;cursor:pointer;" >
		<center>
			<image  src="<?php echo base_url();?>design/images/register.jpg" style="cursor:pointer;">
			<h3 class="post-subtitle"> Register</h3>
		</center>
		</div>
		</a>
		
		<a href="<?php echo base_url()?>welcome/shedule">
		<div class="hvr-shrink" style="cursor:pointer" >
		<center>
			<image  src="<?php echo base_url();?>design/images/calendar.png" style="width:150px;height:150px;cursor:pointer;">
			<h3 class="post-subtitle"> Schedule</h3>
		</center>
		</div>
		</a>
		
		<a href="<?php echo base_url()?>welcome/dashboard">
		<div class="hvr-shrink" style="float:right;cursor:pointer" >
		<center>
			<image  src="<?php echo base_url();?>design/images/dash.png" style="width:150px;height:150px;cursor:pointer;" >
			<h3 class="post-subtitle"> Dashboard</h3>
		</center>
		</div>
		</a>
		<br/><br/><br/>
		<hr>
			
       <div >
		
		
		<h1 class="intro-header.heading"> Tech News </h1>
		
		<br><br/>
		<div style="float:left"> C|NET - Latest Technology News</br>
		<iframe width="520" height="315" src="https://www.youtube.com/embed/bzzKD42k-Ec?list=PL604E9D76028044EA" frameborder="0" allowfullscreen></iframe>
		</br>
		            <a class="twitter-timeline" style="width:550"  href="https://twitter.com/TechCrunch" data-widget-id="710343459190607872">Tweets by @TechCrunch</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
     
		
		</div>
		<div style="float:right" >
		</br>
					<div id="feedControl1" style="height:280">NYT > Tech</div>	
					</br>
					<div id="feedControl">BBC > Tech</div>
					</br>
					<div style="width:440px" id="feedControl3">CNN > Tech</div>
				</br>
					<div style="width:440px" id="feedControl2">TOI > Tech</div>
				
		</div>
		
<div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content" style="width: 820px;height: 116px;margin-left: -6pc;margin-top: 10pc;">
                
                <div class="modal-body">
                  <p>Login Successfull !!! 
                   <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times fa-2x"></i></button></p>
                 
                </div>
                
              </div>
              
            </div>
          </div>
    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<hr>
                    <p class="copyright text-muted">Copyright &copy; Hewlett Packard Enterprise 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    

</body>

</html>

