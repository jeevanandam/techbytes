<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TechBytes- Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>design/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>design/css/clean-blog.min.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body >

<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="ourindex.php" ><img src="<?php echo base_url();?>design/images/hpe.png" style="width:199px;height:84px;cursor:pointer;"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo base_url();?>welcome/index">Home</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>welcome/register">Register</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>welcome/sheduler">Sheduler</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>welcome/login">Login</a>
                </li>
                <li>
                    <?php ?>
                    <a href="<?php echo base_url();?>welcome/dashboard">Hi Shilpa !</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('<?php echo base_url();?>design/images/reg2.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Login</h1>
                    <hr width= "100%"class="large">
                    <span class="subheading">Login to view your Tech Bytes session</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="col-md-7 col-xs-12">
    <div class="x_content">
        <br />
        <form  id="demo-form2" method="post" action="<?php echo base_url();?>dashboard/login" class="form-horizontal" enctype="multipart/form-data" >

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email ID <span class="required"><font color="Red">*</font></span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Name your presentation">
                </div>
            </div>

            <div class="form-group">
                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Password<span class="required"><font color="red">*</font></span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="middle-name" required="required" class="form-control col-md-7 col-xs-12" type="password" name="middle-name" placeholder="describe it">
                </div>
            </div>



            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="button" class="btn btn-primary" id="cancel">Cancel</button>
                    <button type="submit" id="submit" class="btn btn-success">Login</button>
                    <script src="http://code.jquery.com/jquery-2.0.3.js"></script>
                    <script src="<?php echo base_url();?><design/js/bootstrap1.js"></script>
                    <script src="<?php echo base_url();?><design/js/bootstrap-dialog2.js"></script>
                </div>
            </div>


            <!--                <div class="form-group">-->
            <!--                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Upload a file <span class="required"><font color="Red">*</font></span>-->
            <!--                      </label>-->
            <!--                      <div class="col-md-6 col-sm-6 col-xs-12">-->
            <!--                        <input type="file" required="required" class="form-control" id="file" >-->
            <!--                      </div>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="ln_solid"></div>-->
            <!--                <div class="form-group">-->
            <!--                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"><center>-->
            <!--                        <button type="button" class="btn btn-primary" id="cancel"><center>Cancel</center></button>-->
            <!--                        <button type="" id="submit" class="btn btn-success">Submit</button>-->
            <!--						<script src="http://code.jquery.com/jquery-2.0.3.js"></script>-->
            <!--                          <script src="--><?php //echo base_url();?><!--design/js/bootstrap1.js"></script>-->
            <!--                          <script src="--><?php //echo base_url();?><!--design/js/bootstrap-dialog2.js"></script>-->
            <!--						</center>-->
            <!--                      </div>-->
            <!--                    </div>-->


        </form>

    </div>
</div>


</div>

<hr>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <hr>
                <p class="copyright text-muted">Copyright &copy; Hewlett Packard Enterprise 2016</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="<?php echo base_url();?>design/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>design/js/bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url();?>design/js/clean-blog.min.js"></script>


<script>


    var text= '<p>Successfully .  what do you wanna do now?</p>'

    //	 document.getElementById("submit").onclick = function()
    //								{
    //									BootstrapDialog.show({
    //
    //										message: text,
    //										buttons: [{
    //											label: 'go to Dashboard',
    //											 cssClass: 'btn-primary',
    //											action: function(dialog) {
    //												window.location.href = "dashboard3.php";
    //												dialog.close();
    //											}
    //										}, {
    //											label: 'Do nothing',
    //											action: function(dialog) {
    //												window.location.href = "ourindex.php";
    //												dialog.close();
    //											}
    //										}]
    //									});
    //
    //	}
    //

</script>
<script>
//    $(document).ready(function (){
//        $('#submit').click(function() {
//            var last=$("#last-name").val();
//            var middle=$("#middle-name").val();
//            var dates=$("#date").val();
//            var multiple=$("#multiple").val();
//            var myfile =$("#fil").val();
//            var form_key  =  '<?php //echo base_url(); ?>//dashboard/index';
//            $.ajax({
//                url: form_key,
//                type: 'POST',
//                mimeType:"multipart/form-data",
//                data: {last_name: last,mid:middle,dat:dates,multi:multiple,myfil:myfile},
//                dataType: 'html',
//                success: function(data) {
//                    alert(data);
//
//                }
//
//
//            });
//
//        });
//
//    });

</script>
</body>

</html>
