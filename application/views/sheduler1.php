

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TechBytes- shedules</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>design/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>design/css/clean-blog.min.css" rel="stylesheet">
	

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.5sh5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script type="text/javascript" src="<?php echo base_url();?>design/js/sortable.js"></script>
	<script src="<?php echo base_url();?>design/js/bootstrap.js"></script>
</head>

<body >

    <!-- Navigation -->
   <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
       
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>welcome/home" ><img src="<?php echo base_url();?>design/images/hpe.png" style="max-width:60%;cursor:pointer;" class="img-responsive"/></a>
            </div>
            <?php $username=$this->session->userdata('firstname');?>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url();?>welcome/index">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>welcome/register">Register</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>welcome/sheduler">Sheduler</a>
                    </li>
                    <li>
                    <a href="<?php echo base_url();?>welcome/logout">Logout</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>welcome/dashboard">Hi <?php echo $username;?> !</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('<?php echo base_url();?>design/images/event2.png')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>View Schedules</h1>
                        <hr width= "100%"class="large">
                        <span class="subheading" >Browse through past , present and Future shedules</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="x_content">
                  <br />
                
				
						<style>
						  table {
							float:center;
							width:80%;
							
						  }
						  table, td, th {    
    
                           text-align: center;
							}

						  th, td{
						  padding: 15px;
						  }
						 
						</style>
						
						<center> <style>
							thead {color:white;}
							
							tfoot {color:red;}
							
							
							}
				</style>
				<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				<form  method="post">
					
					<p style="width=10px;height:10px;"<i class="glyphicon glyphicon-search"></i><input type="text" name="search" title="search here for the schedules" class="search_button" placeholder="search" id="search" style="autocomplete:off;autocapitalize:off;autocorrect:off;background-color:#d1d1e0;"></input>
						
				</form>	</span>
				
				<table class="sortable table-unstriped responsive-utilities jambo_table" id="mytable"><thead>
					  <thead style="background-color:#001a33;>
                      <tr  class="headings" >
						<th ><center> S.No&nbsp </center></th>
						<th><center> Presenter&nbsp </center></th>
                        <th><center> Topic&nbsp </center></th>
						<th><center> Slot&nbsp </th>
						<th><center> Download&nbspmaterial </center></th>
						
						<th class=" no-link last"><span class="nobr">
							
                        </th>
						
                        
                      </tr>
					  
					  </thead>
					  <tbody><center>
						  <?php $i=1; foreach($getcollection as $get){
							  $app=$get->approve;
							  if($app=='approved'){?>
					  <tr class="even pointer">
						<td class=" "><?php echo $i; $i++;?></td>
                        <td  style="display:none;" class=""><?php echo $get->emp_id;?></td>
						<td class=" "><?php echo $get->empname;?></td>
                        <style>
								.planbtn { background-color :#EFF4F6 ;
											padding : 0;
											background: none;
										   border: none ;
										   outline : none;
										 }
										 .planbtn
							</style>
							<td ><button id="plan" class="planbtn" ><u><?php echo $get->topic;?></u></button>
							
							<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
								<script src="<?php echo base_url();?>design/js/bootstrap1.js"></script>
								<script src="<?php echo base_url();?>design/js/bootstrap-dialog2.js"></script>
								<script>
							var text= '<p> Topic : Planning <br> Description : Planning the business process <br> Attachment : <u>shilpa.ppt</u></p>'

							</script>
							</td>
							<td><?php echo $get->date_time;?></td>
							<style>
							.button {background-color: #00CC99;
                                              color: white;
                                              padding: 5px;
                                              font-size: 16px;
                                              border: shadow;
                                              cursor: pointer;
											  
									}
						</style>
							<td class=" ">
							<a href="<?php echo $get->pdf;?>"><u><font size="4px"><center>
                                <?php $string=$get->pdf;
                                 $va=explode('/', $string, 6);
                                 $va[5];
echo substr($va[5],0,-4)?></center></font></u></a> </td>
							<!--<button onclick="location.href = 'feedb';" id="mailBtn" class="button" type="button" title="click to view feedback"; >give&nbspFeedback</button> -->
                        </tr>

						  <?php }} ?>
						</center></tbody>
						</table> </center>
						</div>
						<body>
                </div>
	</div>
    <hr>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<hr>
                    <p class="copyright text-muted">Copyright &copy; Hewlett Packard Enterprise 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>design/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>design/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>design/js/clean-blog.min.js"></script>

	</>
