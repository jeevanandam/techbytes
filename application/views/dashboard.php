<!DOCTYPE html>
<html lang="en">



<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Tech Bytes </title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url();?>design/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url();?>design/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>design/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url();?>design/css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>design/css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="<?php echo base_url();?>design/css/icheck/flat/green.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>design/css/floatexamples.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
                <script src="<?php echo base_url();?>design/js/bootstrap1.js"></script>
                <script src="<?php echo base_url();?>design/js/bootstrap-dialog2.js"></script>

<script src="<?php echo base_url();?>design/js/pace/pace.min.js"></script>

  <!-- skycons -->
  <script src="<?php echo base_url();?>design/js/skycons/skycons.min.js"></script>


  <script src="<?php echo base_url();?>design/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>design/js/nprogress.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>design/js/sortable.js"></script>

<style>
<style>
              
            
.Approve { background-color: #00CC99;
                                              color: white;
                                              padding: 5px;
                                              font-size: 16px;
                                              border: shadow;
                                              cursor: pointer;
                                            }

.viewfe { background-color: #00CC99;
                                              color: white;
                                              padding: 5px;
                                              font-size: 16px;
                                              border: shadow;
                                              cursor: pointer;
                                            }
                  .Approve:hover, .Approve:focus { background-color: #000000;} ,
                  .dropdown {  position: relative;
                                               display: inline-block;  }
                .dropdown-content { display: none;
                                                    position: absolute;
                          background-color: #989898;
                          min-width: 80px;
                          overflow: auto;
                          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); }
                  .dropdown:hover .dropdown-content {display : block;}

                .dropdown-content a {
                  color: black;
                  padding: 5px;
                  text-decoration: none;
                  display: block;
                }

                .dropdown-content a:hover {background-color: #f1f1f1}

                .show {display:block;}


              </style>
</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
		<a class="navbar-brand" href="<?php echo base_url();?>welcome/home" ><img src="<?php echo base_url();?>design/images/hpe.png" style="width:100px;height:42px;cursor:pointer;"/></a>

          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">

              <a href="<?php echo base_url();?>welcome/dashboard"><img src="<?php echo base_url();?>design/images/images.png" alt="<?php echo base_url();?>design/images/user.png" class="img-circle profile_img" style="width:55px;height:55px;"></a>


			</div>
            <?php $username=$this->session->userdata('firstname');?>
            <div class="profile_info">
              <p>Welcome,<h2><?php echo $username ?> </h2></p>

            </div>
          </div>
          <!-- /menu prile quick info -->



          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <hr width="0%">

              <ul class="nav side-menu">
                <li><a href="<?php echo base_url();?>welcome/home"><i class="fa fa-home"></i> Home </a>

                </li>
                <li><a href="<?php echo base_url();?>welcome/register"><i class="fa fa-edit"></i> Register </a>
                </li>
                <li><a href="<?php echo base_url();?>welcome/dashboard"><i class="fa fa-desktop"></i> Presenter's View </a>

                </li>
                <li><a href="<?php echo base_url();?>welcome/sheduler"><i class="fa fa-table"></i> Sheduler </a>

                </li>

              </ul>
            </div>


          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">

            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
           <div class="navbar nav_title" style="border: 0;background-color:#f5f5f0;">
            <a href="<?php echo base_url() ?>" class="site_title"> <span><font color="black"><b>TechBytes!</b></font></span></a>
          </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <div style="text-transforamtion" >  <img src="<?php echo base_url();?>design/images/images.png" alt=""><?php echo $username ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
				<li>
                    <a href="<?php echo base_url() ?>">

                      <span>Home</span>
                    </a>
                  </li>
                  <li><a href="<?php echo base_url()?>welcome/dashboard">  Dashboard</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>welcome/sheduler">

                      <span>Sheduler</span>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()?>welcome/register">Register</a>
                  </li>
                   <li>
                    <a href="<?php echo base_url()?>welcome/logout">Logout</a>
                  </li>

                </ul>
              </li>
            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">

        <!-- top tiles -->

        <!-- /top tiles -->

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>List of Presenters and topics <small>Unapproved , Approved and Rejected</small></h3>
                </div>
                <div class="col-md-6">

                </div>
              </div>

              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="x_content">
                  <table id="example" class="table table-unstriped responsive-utilities jambo_table" >
				  <caption><center><b>UnApproved List</b></center> </caption>
				  <thead>

                      <tr class="headings">
						<th><center>S.no </center></th>
                        <th><center>Presentor</center></th>
                        <th><center>Topic</center></th>
                        <th><center>Slot</center></th>
						  <th><center>Date</center></th>
                        <th><center>Respond </center></th>

                        <th class="no-link last"><center><span class="nobr">
							<form id="searchform" action="contact.html" method="post" >
							<i class="glyphicon glyphicon-search"></i>
								<input type="text" name="search" class="search_button" placeholder="search" id="search" style="autocomplete:off;autocapitalize:off;autocorrect:off;background-color:black;">

							</form>	</span></center>
                        </th>
                      </tr>

                    </center></thead>
					<tbody>
					<?php $i=1;foreach ($getcollection as $get){
					$app=$get->approve;
					if($app =='unapproved'){?>
					<tr class="even pointer"><center>
            <td ><center><?php echo $i; $i++;?></center></td> 
						<td  style="display:none;" class="serialnum"><center><?php  echo $get->reg_id;?></center></td>
                        <td class=" "><center><?php echo $get->empname;?></center></td>
						<td ><center>
						<button type="button" class="planbtn" id="plan" style="background-color:#EFF4F6; outline:none; border:none; padding:0;" title="click to continue reading description">
						<u><?php echo $get->topic;?></u></button>
								
                        </center></td>
						<td class=" "><center><?php echo $get->slot;?></center></td>
							<td class=" "><center><?php echo $get->date_time;?></center></td>
                        <td class="last"><center><style>.dropbtn { background-color: #00CC99;
                                              color: white;
                                              padding: 5px;
                                              font-size: 16px;
                                              border: shadow;
                                              cursor: pointer;
                                            }


								  .dropbtn:hover, .dropbtn:focus { background-color: #000000;} ,
								  .dropdown {  position: relative;
                                               display: inline-block;  }
								.dropdown-content { display: none;
                                                    position: absolute;
													background-color: #989898;
													min-width: 80px;
													overflow: auto;
													box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); }
									.dropdown:hover .dropdown-content {display : block;}

								.dropdown-content a {
									color: black;
									padding: 5px;
									text-decoration: none;
									display: block;
								}

								.dropdown-content a:hover {background-color: #f1f1f1}

								.show {display:block;}
              .Approve {
                      background-color: #00CC99;
                      color: white;
                      padding: 5px;
                      font-size: 16px;
                      border: shadow;
                      cursor: pointer;
                  }

							</style>
						        <div class="dropdown">
								  <button id="approve" class="Approve" title="click to view options">Approve</button>

								</div>



							</center></td>
							<td >
								<div class="dropdown">
									<button id="reject"  class="dropbtn" title="click to view options">Rejects</button>

								</div>
							</td>


                      </center>
					</tr>
					<?php }} ?>
					  </tbody>
					</table>

			</div>
			</div>

			</div>

              </div>
			  </div>
			  <div class="row">
              <div class="col-md-10 col-sm-10 col-xs-10 bg-white">

               <hr width="0%">
			<hr width="0%">


			<div class="x_content">
                  <table id="table1" class="table table-unstriped responsive-utilities jambo_table" >
				  <caption><center><b>Approved List</b></center> </caption>
				  <thead>
                      <tr class="headings">
						<th><center>S.no </center></th>
                        <th><center>Presentor</center></th>
                        <th><center>Topic</center></th>
                        <th><center>Slot</center></th>
                        <th><center>&nbspfeedback </center></th>
                        <th><center>view&nbspfeedback </center></th>
						<th><center>close&nbspsession?<center></th>
                        <th class="no-link last"><center><span class="nobr">
							<form action="contact.html" method="post" >
								<i class="glyphicon glyphicon-search"></i>
								<input type="text" name="search" class="search_button" placeholder="search" id="search" style="autocomplete:off;autocapitalize:off;autocorrect:off;background-color:black;">

							</form>	</span></center>

                        </th>
                      </tr>
                    </thead>
					<tbody>
					<?php $i=1; foreach ($getcollection as $get){
						 $app=$get->approve;
						if($app =='approved'){
						?>
					<tr class="even pointer">
            <td ><center><?php echo $i; $i++;?></center></td> 
						<td style="display:none;" class="register_id"><center><?php echo $get->reg_id;?></center></td>
                        <td class=" "><center><?php echo $get->empname;?></center></td>
						<td >
						<button type="button" class="planbtn" id="plan1" style="background-color:#EFF4F6; outline:none; border:none; padding:0;" title="click to continue reading description"><u><?php echo $get->topic?></u></button>


                        </td>
						<td class=" "><center><?php echo $get->slot?></center></td>


				

						 <!-- <form method="post" action="mailto: <?php foreach($getcollection as $col){ echo $get->email.';';};?>?subject=feedback&body=hi,
											please give us feedback
											www.google.com">
						<input type ="submit" class="dropbtn" value="send feedback mail"  />

						</form> -->
            <td class=" "><button id="send_feedback" class="sendfe" type="button" title="click to send mail">Send&nbspfeedback</button></td>

                       </center></td>
                        <td><center>

						<button id="feedback" class="viewfe" type="button" title="click to send mail">view&nbspfeedback</button>


                        </center></td>
						<td class="last"><center>
						<a href="#"><button class="delete" type="button" id="close">Close</button></a>

							
						</center></td>
            <style>
              .delete {
                      background-color: #00CC99;
                      color: white;
                      padding: 5px;
                      font-size: 16px;
                      border: shadow;
                      cursor: pointer;
                  }
                  .dropbtn {
                      background-color: #00CC99;
                      color: white;
                      padding: 5px;
                      font-size: 16px;
                      border: shadow;
                      cursor: pointer;
                  }
                  .sendfe
                   {
                      background-color: #00CC99;
                      color: white;
                      padding: 5px;
                      font-size: 16px;
                      border: shadow;
                      cursor: pointer;
                  }
            </style>
                      </tr>

					<?php } }?>
					  </tbody>
					</table>

			</div>

              </div>
			</div>
			  <!-- Rejected list table -->

			  <div class="row">
			   <div class="col-md-10 col-sm-10 col-xs-10">
			    <hr width="0%">
			<hr width="0%">
			<div class="x_content">
                  <table id="table1" class="table table-unstriped responsive-utilities jambo_table" >
				  <caption><center><b>Rejected List</b></center> </caption>
				  <thead><center>
                      <tr class="headings">
						<th><center>S.no </center></th>
                        <th><center>Presentor</center></th>
                        <th><center>Topic</center></th>
                        <th><center>Slot</center></th>

                       <!-- <th><center>Respond </center></th> -->
						<th><center> Date</center></th>
                        <th class="no-link last"><span class="nobr">
							<form action="contact.html" method="post" >
								<i class="glyphicon glyphicon-search"></i>
								<input type="text" name="search" class="search_button" placeholder="search" id="search" style="autocomplete:off;autocapitalize:off;autocorrect:off;background-color:black;">

							</form>	</span>
                        </th>
                      </tr>
					  </center>
                    </thead>
					<tbody><center>
						<?php $i=1; foreach ($getcollection as $get){
						$app=$get->approve;
						if($app =='rejected'){
						?>
					<tr class="even pointer">
						<td ><center><?php echo $i; $i++;?></center></td> 
            <td style="display:none;" class=" "><center><?php echo $get->reg_id;?></center></td>
                        <td class=" "><center><?php echo $get->empname ?></center></td>
						<td >
						<button type="button" class="planbtn" id="plan2" style="background-color:#EFF4F6; outline:none; border:none; padding:0;"title="click to continue reading description"><u><center><?php echo $get->topic;?></center></u></button>


                        </td>
						<td class=" "><center><?php echo $get->slot ?></center></td>

						<td><center><?php echo $get->date_time ?></center></td>

						
                        <td class=" last"><center>


                        </center></td>
                      </tr>
					  <?php } } ?>
					  </tbody>
					</table>


			</div>

			</div>
              <div class="clearfix"></div>
            </div>
          </div>


        <br />

        <!-- footer content -->


        <!-- /footer content -->
      </div>
      <!-- /page content -->

    </div>

  </div>


  
  


  <!-- dashbord linegraph -->

  <!-- /dashbord linegraph -->
  <!-- datepicker -->
  <script type="text/javascript">

	// Get the button, and when the user clicks on it, execute myFunction
							document.getElementById("myBtn","mailBtn").onclick = function() {myFunction()};

							/* myFunction toggles between adding and removing the show class, which is used to hide and show the dropdown content */
							function myFunction() {
								document.getElementById("myDropdown").classList.toggle("show");
							}

							// Close the dropdown if the user clicks outside of it
							window.onclick = function(event) {
							  if (!event.target.matches('.dropbtn')) {

								var dropdowns = document.getElementsByClassName("dropdown-content");
								var i;
								for (i = 0; i < dropdowns.length; i++) {
								  var openDropdown = dropdowns[i];
								  if (openDropdown.classList.contains('show')) {
									openDropdown.classList.remove('show');
								  }
								}
							  }
							}



					// code for planning  button in unapproved list

					var text= '<p> Topic : planning <br> Description : planning the business process <br> attachment : <u>shilpa.ppt</u></p>'
								 document.getElementById("plan").onclick = function()
								{
									BootstrapDialog.show({
										message: text,
										buttons: [{
										label : 'close',
										action: function(dialog) {
												dialog.close();
												}
										}]
									});
								  }

					// code for planning button in approved list

					var text= '<p> Topic : planning <br> Description : planning the business process <br> attachment : <u>shilpa.ppt</u></p>'
								 document.getElementById("plan1").onclick = function()
								{
									BootstrapDialog.show({
										message: text,
										buttons: [{
										label : 'close',
										action: function(dialog) {
												dialog.close();
												}
										}]
									});
								  }
								 // code for planning button in rejected list

					var text= '<p> Topic : planning <br> Description : planning the business process <br> attachment : <u>shilpa.ppt</u></p>'
								 document.getElementById("plan2").onclick = function()
								{
									BootstrapDialog.show({
										message: text,
										buttons: [{
										label : 'close',
										action: function(dialog) {
												dialog.close();
												}
										}]
									});
								  }



										  // code for approve in Rejected table
							document.getElementById("approve1").onclick = function()
								{

								 BootstrapDialog.show({

										message: 'Do you really want to Approve ?',
										buttons: [{
											label: 'Yes',
											 cssClass: 'btn-primary',
											action: function(dialog) {
													alert('approved');
												dialog.close();
											}
										}, {
											label: 'No',
											action: function(dialog) {
												//dialog.setMessage('Message 2');
												dialog.close();
											}
										}]
									});

									//BootstrapDialog.confirm('Are you sure you want to Approve?');
								}





						// code for close

							var text1= '<p>Are you sure you want to close?</p>'
								 document.getElementById("close").onclick = function()
								{


								 BootstrapDialog.show({

										message: 'Do you really want to cancel ?',
										buttons: [{
											label: 'Yes',
											 cssClass: 'btn-primary',
											action: function(dialog) {
											var button = document.getElementById("close");
											 button.innerText = 'closed';
											 button.style.background ='gray';
                                              button.disabled = true;

												dialog.close();
											}
										}, {
											label: 'No',
											action: function(dialog) {
												//dialog.setMessage('Message 2');
												dialog.close();
											}
										}]
									});


								}

								var text2= '<p>choose approve or reject</p>'
								 document.getElementById("myBtn").onclick = function()
								{


								 BootstrapDialog.show({

										message: text2,
										buttons: [{
											label: 'Approve',
											 cssClass: 'btn-primary',
											cssid:'approve',
											action: function(dialog) {

											alert('approved');
												dialog.close();
											}
										}, {
											label: 'Reject',
											action: function(dialog) {

												dialog.close();
											}
										}]
									});


								}


					var text4= '<p> Title : good <br> review:need to imporve quality <br>rating : 3</p>'
								 document.getElementById("feedback").onclick = function()
								{
									BootstrapDialog.show({
										message: text4,
										buttons: [{
										label : 'close',
										action: function(dialog) {
												dialog.close();
												}
										}]
									});
								  }



  </script>
  <script>
    NProgress.done();
  </script>
  <script>
	  $(".Approve").click(function() {
		  var $row = $(this).closest("tr");    // Find the row
		  var text = $row.find(".serialnum").text(); // Find the text
		  var form_key="<?php echo base_url()?>dashboard/approve";
		  $.ajax({
                url: form_key,
                type: 'POST',
                mimeType:"multipart/form-data",
                data: {reg_id:text},
                dataType: 'html',
                success: function(data) {

				 location.reload();

               }
				});
	  });

    $(".dropbtn").click(function() {
        var $row = $(this).closest("tr");    
        var text = $row.find(".serialnum").text(); 
        var form_key="<?php echo base_url()?>dashboard/reject";
        $.ajax({
           url: form_key,
           type: 'POST',
           mimeType:"multipart/form-data",
           data: {register_id:text},
           dataType: 'html',
           success: function(data) {

             location.reload();
           }
         });
    });


      $('.delete').click(function() {
        var $row = $(this).closest("tr");    
        var test = $row.find(".register_id").text(); 
        var txt= test.trim();
        var forms_key="<?php echo base_url()?>dashboard/deleterow";
            $.ajax({
               url: forms_key,
               type: 'POST',
              // mimeType:"multipart/form-data",
               data: {register_id:txt},
               dataType: 'html',
               success: function(data) {
                location.reload();
               }
             });

      });

      $('.sendfe').click(function() {
        var $row = $(this).closest("tr");    
        var test = $row.find(".register_id").text(); 
        var txt= test.trim();
        var forms_key="<?php echo base_url()?>feedback/sendmail";
            $.ajax({
               url: forms_key,
               type: 'POST',
              // mimeType:"multipart/form-data",
               data: {register_id:txt},
               dataType: 'html',
               success: function(data) {
               // location.reload();
               }
             });
      });

      $('.viewfe').click(function() {
        var $row = $(this).closest("tr");    
        var test = $row.find(".register_id").text(); 
        var txt= test.trim();
        var forms_key="<?php echo base_url()?>feedback/viewfeedback";
            $.ajax({
               url: forms_key,
               type: 'POST',
              // mimeType:"multipart/form-data",
               data: {register_id:txt},
               dataType: 'html',
               success: function(data) {
               // location.reload();
               }
             });
      });

</script>



</body>

</html>
