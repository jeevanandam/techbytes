<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TechBytes- Feedback</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>design/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>design/css/clean-blog.min.css" rel="stylesheet">
	

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body >

    <!-- Navigation -->
   <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
       
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="ourindex.php" ><img src="<?php echo base_url();?>design/images/hpe.png" style="max-width:60%;cursor:pointer;" class="img-responsive"/></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="ourindex.php">Home</a>
                    </li>
                    <li>
                        <a href="register.php">Register</a>
                    </li>
                    <li>
                        <a href="sheduler1.php">Sheduler</a>
                    </li>
                    <li>
                        <a href="dashboard3.php">Hi Shilpa !</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <header class="intro-header" style="background-image: url('<?php echo base_url();?>design/images/reg2.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>Feedback</h1>
                        <hr width= "100%"class="large">
                        <span class="subheading">Your feedback improves our maintanance! Any kind of feedback is highly appreciated</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="x_content">
                  <br />
				 
                  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" >
				  <div class="form-group">
				   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
					  <label>Title of the Review<font color="Red">*</font></label>
                        <input type="text" id="comments" rows="3" required="required" class="form-control col-md-7 col-xs-12" placeholder="give a title for review"></textarea>
                      </div>
				  </div>
				    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required"></span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
					  <label>Comments</label>
                        <textarea id="comments" rows="3" class="form-control col-md-7 col-xs-12" placeholder="comment about presenter"></textarea>
                      </div>
                    </div>
					
					<div class="form-group">
					<form id="ratingsForm" action=>
					<label class="control-label col-md-5 col-sm-5 col-xs-12" ><b><font size="5px">Rate us</font></b><span class="required"></span>
					 </label>
							
								<div class="stars">
									
									<input type="radio" name="star" checked=" " class="star-1" id="star-1" />
									<label class="star-1" for="star-1">1</label>
									<input type="radio" name="star" class="star-2" id="star-2" />
									<label class="star-2" for="star-2">2</label>
									<input type="radio" name="star" class="star-3" id="star-3" />
									<label class="star-3" for="star-3">3</label>
									<input type="radio" name="star" class="star-4" id="star-4" />
									<label class="star-4" for="star-4">4</label>
									<input type="radio" name="star" class="star-5" id="star-5" />
									<label class="star-5" for="star-5">5</label>
									<span></span>
								</div>
							  
							</form>
					
					</div>
					
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <center>
                        <button type="submit" id="cancel" class="btn btn-primary"><center>Cancel</center></button>
                        <button type="button" id="submit" class="btn btn-success">Submit</button>
						<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
								<script src="<?php echo base_url();?>design/js/bootstrap1.js"></script> 
								<script src="<?php echo base_url();?>design/js/bootstrap-dialog2.js"></script> 
						</center>
                      </div>
                    </div>
			    </fieldset>
                  </form>
				 
                </div>

  
<hr>
    <!-- Footer -->
     <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<hr>
                    <p class="copyright text-muted">Copyright &copy; Hewlett Packard Enterprise 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>design/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>design/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>design/js/clean-blog.min.js"></script>

	
	<script>
	
	
	var text= '<p>Your feedback successfully registered</p>'
	
	 document.getElementById("submit").onclick = function()
								{
									BootstrapDialog.show({
            
										message: text,
										buttons: [{
											label: 'OK',
											 cssClass: 'btn-primary',
											action: function(dialog) {
												window.location.href = "user_feedback.html";
												dialog.close();
											}
										
										}]
									});
									}
									document.getElementById("cancel").onclick = function()
									{
									window.location.href = "feedback_presenter.html";
									}
								  
	
	</script>
	
	
	<style>
	form .stars {
  background: url("images/stars.png") repeat-x 0 0;
  width: 150px;
  margin: 0 auto;
}

form .stars input[type="radio"] {
  position: absolute;
  opacity: 0;
  filter: alpha(opacity=0);
}
form .stars input[type="radio"].star-5:checked ~ span {
  width: 100%;
}
form .stars input[type="radio"].star-4:checked ~ span {
  width: 80%;
}
form .stars input[type="radio"].star-3:checked ~ span {
  width: 60%;
}
form .stars input[type="radio"].star-2:checked ~ span {
  width: 40%;
}
form .stars input[type="radio"].star-1:checked ~ span {
  width: 20%;
}
form .stars label {
  display: block;
  width: 30px;
  height: 30px;
  margin: 0!important;
  padding: 0!important;
  text-indent: -999em;
  float: left;
  position: relative;
  z-index: 10;
  background: transparent!important;
  cursor: pointer;
}
form .stars label:hover ~ span {
  background-position: 0 -30px;
}
form .stars label.star-5:hover ~ span {
  width: 100% !important;
}
form .stars label.star-4:hover ~ span {
  width: 80% !important;
}
form .stars label.star-3:hover ~ span {
  width: 60% !important;
}
form .stars label.star-2:hover ~ span {
  width: 40% !important;
}
form .stars label.star-1:hover ~ span {
  width: 20% !important;
}
form .stars span {
  display: block;
  width: 0;
  position: relative;
  top: 0;
  left: 0;
  height: 30px;
  background: url("<?php echo base_url();?>design/images/stars.png") repeat-x 0 -60px;
  -webkit-transition: -webkit-width 0.5s;
  -moz-transition: -moz-width 0.5s;
  -ms-transition: -ms-width 0.5s;
  -o-transition: -o-width 0.5s;
  transition: width 0.5s;
}
</style>

  
</body>

</html>
