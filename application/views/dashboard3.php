<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Tech Bytes-Presenter's view </title>

  <!-- Bootstrap core CSS -->
  <style>
.planbtn { background-color :#EFF4F6 ;
padding : 0;                                             background: none;
border: none ;                                            outline : none;
}             
.dropbtn { background-color: #00CC99;
                                              color: white;
                                              padding: 5px;
                                              font-size: 13px;
											  font-family:Arial;
                                              border: shadow;
                                              cursor: pointer;
                                            }


								  .dropbtn:hover, .dropbtn:focus { background-color: #000000;} ,
								  .dropdown {  position: relative;
                                               display: inline-block;  }
								.dropdown-content { display: none;
                                                    position: absolute;
													background-color: #989898;
													min-width: 80px;
													overflow: auto;
													box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); }
									.dropdown:hover .dropdown-content {display : block;}

								.dropdown-content a {
									color: black;
									padding: 5px;
									text-decoration: none;
									display: block;
								}

								.dropdown-content a:hover {background-color: #f1f1f1}

								.show {display:block;}                </style> 

  <link href="<?php echo base_url();?>design/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url();?>design/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>design/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url();?>design/css/custom.css" rel="stylesheet">

 
  <link href="<?php echo base_url();?>design/css/floatexamples.css" rel="stylesheet" type="text/css" />

  <script src="<?php echo base_url();?>design/js/jquery.min.js"></script>
  <script src="<?php echo base_url();?>design/js/nprogress.js"></script>

<script src="http://code.jquery.com/jquery-2.0.3.js"></script>
	
	 <script src="http://code.jquery.com/jquery-2.0.3.js"></script>
	
 <script src="http://code.jquery.com/jquery-2.0.3.js"></script>
								<script src="<?php echo base_url();?>design/js/bootstrap1.js"></script>
								<script src="<?php echo base_url();?>design/js/bootstrap-dialog2.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
			<a class="navbar-brand" href="<?php echo base_url();?>welcome/home" ><img src="<?php echo base_url();?>design/images/hpe.png" style="width:100px;height:42px;cursor:pointer;"/></a>
          
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
         <div class="profile">
            <div class="profile_pic">
			
              <a href="<?php echo base_url();?>welcome/dashboard"><img src="<?php echo base_url();?>design/images/images.png" alt="<?php echo base_url();?>design/images/images.png" class="img-circle profile_img" style="width:50px;height:50px;"></a>
            
				
			</div>
            <div    style="text-transform: capitalize;"  class="profile_info">
              <p>Welcome,<h2><?php echo  $username=$this->session->userdata('firstname');?></h2></p>
              
            </div>
          </div>
          <!-- /menu prile quick info -->

          <!--<br />-->

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <hr width="0%">

              <ul class="nav side-menu">
                <li><a href="<?php echo base_url();?>welcome/home"><i class="fa fa-home"></i> Home </a>

                </li>
                <li><a href="<?php echo base_url();?>welcome/register"><i class="fa fa-edit"></i> Register </a>
                </li>

                <li><a href="<?php echo base_url();?>welcome/sheduler"><i class="fa fa-table"></i> Sheduler </a>

                </li>
                <li><a href="<?php echo base_url();?>welcome/logout"><i class="fa fa-sign-out"></i> Logout </a>

                </li>

              </ul>
            </div>


          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
		<div class="navbar nav_title" style="border: 0;background-color:#f5f5f0;">
            <a href="<?php echo base_url();?>welcome/home" class="site_title"> <span><font color="black"><b>TechBytes!</b></font></span></a>
          </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="<?php echo base_url();?>design/images/images.png" alt=""><?php echo  $username=$this->session->userdata('firstname');?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
				<li>
                    <a href="<?php echo base_url();?>welcome/home">

                      <span>Home</span>
                    </a>
                  </li>
                  <li><a href="<?php echo base_url();?>welcome/dashboard">  Dashboard</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url();?>welcome/sheduler">

                      <span>Sheduler</span>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url();?>welcome/register">Register</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url();?>welcome/logout">Logout</a>
                  </li>


                </ul>
              </li>
            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">


			<div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="dashboard_graph">

		  <div class="row x_title">
                <div class="col-md-6">
                  <h3>Your list of Presentations <small></small></h3>
                </div>
                <div class="col-md-6">

                </div>
              </div>

			   <div class="col-md-10 col-sm-10 col-xs-10">
			    <div class="x_content">
				  <table id="example" class="table table-unstriped responsive-utilities jambo_table" >
				  <thead>
                      <tr class="headings">
						<th><center>S.No </center></th>
                        <th><center>Topic</center></th>
                        <th></center>Status<center></th>
<!--						<th></center>Change&nbspSlot</center></th>-->
						<th><center>Delete</center></th>
						<th><center>User&nbspFeedback</center> </th>
						<th><center>Upload&nbspAdditional&nbspmaterial</center></th>
						<th><center>Download Meterial</center></th>
                        <th class=" no-link last"><span class="nobr">
							<form action="contact.html" method="post" >
								<input type="search" name="search" placeholder="search">

                            </form> </span>                         </th>
</tr>                     </thead>                     <tbody>

					<?php $l=1; foreach($getdata as $get) {?>        
					             <tr class="evenpointer">                         
						<td class="register_id" style="display:none">
							<?php echo $get->reg_id; ?></td>   
						<td class="" ><font size="4px"><?php echo $l; $l++;?></td>   
                                                  <td>

                    <button id="plan" class="planbtn" ><font size="4px"><?php  echo $get->topic;?><font size="4px"></button>

							
							</td>
                       <!--  <td class=" "><u><a href="BootstrapDialog.show()">planning</a></td></u>  -->
                        <td class=" "><font size="4px"><?php echo $get->approve;?></font>
                        </td>
							<style>
							

							</style>
<!--						<td class=" "><button id="slot" class="delete" type="button" title="click to change slot" ><b>Change&nbspSlot</b></button>-->

						
                        </td>

<style>.delete { background-color: #00CC99;
                                              color: white;
                                              padding: 5px;
                                              font-size: 16px;
                                              border: shadow;
                                              cursor: pointer;
                                            }
                                            </style>

						<td>
							  <button  id="delete" type="button" class="delete" ><b>Delete</b></button>

						
						</td>

						<td class=""><center>

						<button id="feedback" class="dropbtn" type="button" title="click to provide user feedback" ><b>User&nbspFeedback<b/></button>

						</center></td>
						
						<td class=""><center>

						<button type="file" id="upload" class="dropbtn" type="button" title="please upload additional material " ><b>Upload</button>

						</center></td>
						<td class="last"><center>
						<a href="<?php echo $get->pdf;?>"><u><font size="4px"><?php $string=$get->pdf;


                            $va=explode('/', $string, 6);
 $va[5];
echo substr($va[5],0,-4)?></font></u></a>
						</center></td>

                      </tr>
                      <?php } ?>
					  </tbody>
					</table>
				</div>
			   </div>
		  </div>
		  </div>


			</div>



      </div>

	  </div>
              <div class="clearfix"></div>
            </div>




    </div>
	 


  


  <!-- /dashbord linegraph -->
  <!-- datepicker -->
  <script>
                          //plan code
							var text= '<p> <b>Topic : Planning <br> Description : Planning the business process <br> Attachment : <u>shilpa.ppt</u></b></p>'
							document.getElementById("plan").onclick = function()
								{
								BootstrapDialog.show({
										message: text,
										buttons: [{
										label : 'ok',
										action: function(dialog) {
												dialog.close();
												}
										}]
									});
								}

						//cancel code
								var text2 = '<p><b>Do you really want to cancel?</b> </p>'
								document.getElementById("cancel").onclick = function()
								{
								 BootstrapDialog.show({

										message: text2,
										buttons: [{
											label: 'Yes',
											action: function(dialog) {

												//remove entry from data base
												alert('your slot is cancelled');
												dialog.close();
											}
										}, {
											label: 'No',
											action: function(dialog) {

												dialog.close();
											}
										}]
									});


								}


							// slot code
								var text1 = '<p>  <b>Do you really want to change the slot? <b></p>'
								document.getElementById("slot").onclick = function()
								{
								 BootstrapDialog.show({
										
										message: text1,
										buttons: [{
											label: 'Yes',
											cssClass: 'btn-primary',
											action: function(dialog) {
												window.location = 'update.html'


											}
										}, {
											label: 'No',
											action: function(dialog) {

												dialog.close();
											}
										}]
									});


								}

						//user feedback code
					var text4 = new String('<p><b><hr>user:raj <br>Title:good <br> review:has to improve quality An alert dialog box is mostly used to give a warning message to the users. For example, if one input field requires to enter some text but the user does not provide any input, then as a part of validation, you can use an alert box to give a warning message.Nonetheless, an alert </b></p>');
					var text5 =  new String('<p><b><hr>user:sai <br>Title:good <br> review:has to improve quality An alert dialog box is mostly used to give a warning message to the users. For example, if one input field requires to enter some text but the user does not provide any input, then as a part of validation, you can use an alert box to give a warning message.Nonetheless, an alert </b></p>');
					var text6 =  new String('<p><b><hr>user:kiran <br>Title:good <br> review:has to improve quality An alert dialog box is mostly used to give a warning message to the users. For example, if one input field requires to enter some text but the user does not provide any input, then as a part of validation, you can use an alert box to give a warning message.Nonetheless, an alert </b></p>');
					//push data into data base 
					var arr = new Array( text4 , text5, text6);
					
						for (i=0; i<arr.length; i++)
							{
							
							var texts = texts + arr[i] +'<br>Credits =' + i;
							}
							
							
								document.getElementById("feedback").onclick = function()
								{

								BootstrapDialog.show({
										message: texts,
										buttons: [{
										label : 'ok',
										cssClass: 'btn-primary',
										action: function(dialog) {
												dialog.close();
												}
										}]
									});

								}
					var text5 = '<p><input type="file" class="dropbtn" ></input></p>';
				    document.getElementById("upload").onclick = function()
								{
								 BootstrapDialog.show({
										
										message: text5,
										buttons: [{
											label: 'Upload',
											cssClass: 'btn-primary',
											action: function(dialog) {
												//database upload
												dialog.close();

											}
										}, {
											label: 'cancel',
											action: function(dialog) {

												dialog.close();
											}
										}]
									});


								}




  </script>


</body>

</html>
<script type="text/javascript">
		
			$('.delete').click(function() {
				var $row = $(this).closest("tr");    
				var test = $row.find(".register_id").text(); 
				var txt= test.trim();
				var forms_key="<?php echo base_url()?>dashboard/deleterow";
		        $.ajax({
		           url: forms_key,
		           type: 'POST',
		          // mimeType:"multipart/form-data",
		           data: {register_id:txt},
		           dataType: 'html',
		           success: function(data) {
		            location.reload();
		           }
		         });

			});

</script>

