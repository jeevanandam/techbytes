<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper(array('form', 'url','html'));
		$this->load->model('register','',TRUE);
		$this->load->database();
		$this->load->library('email');
	}


	public function index()
	{ 
		$login = $this->session->userdata('userId');
		if($login !='')
		{
			redirect('welcome/home');
		}
		$this->load->view('login');
	}

	public function home()
	{
		$login = $this->session->userdata('userId');
		if($login =='')
		{
			redirect('welcome/index');
		}
		$this->load->view('ourindex');
	}

	public function ourindex()
	{
		$this->load->view('login');
	}

	public function register()
	{
		$this->load->view('register');
	}

	public function sheduler()
	{
		$getcol['getcollection']=$this->register->getunapproved();
		$this->load->view('sheduler1',$getcol);
	}

	public function dashboard()
	{
		$getdata['getdata'] = $this->register->getdata();
		$this->load->view('dashboard3',$getdata);
	}

	public function mydashboard()
	{
		$getcol['getcollection']=$this->register->getunapproved();
		$this->load->view('dashboard',$getcol);
	}

	public function update()
	{

		$this->load->view('update');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('welcome/index');
	}

	public function feedback()
	{
		$id['id']='';
		$id['reg']='';
		$this->load->view('feedback_user',$id);
	}
}
