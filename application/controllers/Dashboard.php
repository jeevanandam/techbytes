<?php
/**
 * Created by PhpStorm.
 * User: codilar
 * Date: 19/3/2016
 * Time: 12:51 AM
 */
class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form', 'url','html'));
        $this->load->model('register','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->database();
        $this->load->library('email');
    }

    public function index()
    {

        $topic = $this->input->post('last_name');
        $date_time =  $this->input->post('dat');
        $desc =  $this->input->post('mid');
        $slot =  $this->input->post('multi');
        $pdf = $this->input->post('myfil');

        $data=array('topic'=>$topic,
            'date_time'=>$date_time,
            'description'=>$desc,
            'slot'=>$slot,
            'pdf'=>$pdf
        );
       // $insert= $this->register->reg($data);





        $base_url= base_url();
        $target_dir = "uploads/";
        if (!file_exists($target_dir)) {
            mkdir("uploads");
        }

        $target_file = $target_dir . basename($pdf);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
         move_uploaded_file($target_file);

         if(move_uploaded_file($pdf, $target_file)) {
                $img_url=$base_url.$target_file;
                $data=array('topic'=>$topic,
                    'date_time'=>$date_time,
                    'description'=>$desc,
                    'slot'=>$slot,
                    'pdf'=>$img_url
                );
                $insert= $this->register->reg($data);
                echo "The file ". basename( $pdf). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }




        }

    public function upload()
    {
        if ($_FILES && $_POST) {

            $topic = $this->input->post('last-name');
            $date_time =  $this->input->post('date');
            $desc =  $this->input->post('middle-name');
            $slot =  $this->input->post('multiple');

            $date['check']=$this->input->post('date');
            $date['slot']=$this->input->post('multiple');

            $checkdata = $this->register->checkdate($date);
            
            if($checkdata != 0){

                $this->session->set_flashdata('slot_error', "Already Slot Engaged, Please Change the Slot ");
                redirect('welcome/register');

            }


            $userId=$this->session->userdata('userId');
            $base_url= base_url();
            $target_dir = "uploads/";
            if (!file_exists($target_dir)) {
                mkdir("uploads");
            }
            $target_file = $target_dir . basename($_FILES["uploadfile"]["name"]);
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            

            if($imageFileType != "ppt" && $imageFileType != "pdf" && $imageFileType != "pptx") {
                $this->session->set_flashdata('format_error', "File Format Should be .ppt or .pdf and pptx");
                redirect('welcome/register');
            }
            
            if (file_exists($target_file)) {

                echo "Sorry, file already exists.";
                
            }


            $uploadOk = 1;
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file

            } else {
                if (move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $target_file)) {
                    $fileurl=$base_url.$target_file;

                    $data=array('topic'=>$topic,
                        'date_time'=>$date_time,
                        'description'=>$desc,
                        'slot'=>$slot,
                        'pdf'=>$fileurl,
                        'user_id'=>$userId
                    );
                    $insertdata = $this->register->reg($data);
                    redirect('welcome/dashboard');

                    echo "The file ". basename( $_FILES["uploadfile"]["name"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
         }
         
    }

    public function update()
    {

        if ($_FILES && $_POST) {

            $topic = $this->input->post('last-name');
            $date_time =  $this->input->post('date');

            $desc =  $this->input->post('middle-name');
            $slot =  $this->input->post('multiple');

            $data=array('topic'=>$topic,
                'date_time'=>$date_time,
                'description'=>$desc,
                'slot'=>$slot,

            );

            print_r($data);die;
            $base_url= base_url();
            $target_dir = "uploads/";
            if (!file_exists($target_dir)) {
                mkdir("uploads");
            }
            $target_file = $target_dir . basename($_FILES["uploadfile"]["name"]);


            $uploadOk = 1;
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file

            } else {
                if (move_uploaded_file($_FILES["uploadfile"]["tmp_name"], $target_file)) {
                    $fileurl=$base_url.$target_file;

                    $data=array('topic'=>$topic,
                        'date_time'=>$date_time,
                        'description'=>$desc,
                        'slot'=>$slot,
                        'pdf'=>$fileurl
                    );
                    $insertdata = $this->register->update($data);
                    redirect('welcome/dashboard');

                    echo "The file ". basename( $_FILES["uploadfile"]["name"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }

    }

    public function login()
    {

        if($_POST){
            $email=$this->input->post('email');
             $password=$this->input->post('emp_id');
            $emailexist = $this->user->emailexist($email);

            if($emailexist == 0){
                
                $em_error="Employe ID Is Not Matching !!.";
                    $this->session->set_flashdata('em_msg', $em_error);
                redirect('welcome/index');
            }else{
                $login = $this->user->login($email,$password);

 

                if($login)
                {

                    $userId = $login[0]->user_id;
                    $email = $login[0]->email;
                    $firstname = $login[0]->empname;
                    $emp_id = $login[0]->emp_id;
                    $flag = $login[0]->flag_data;


                    $data = array(
                        'Error' => 'False',
                        'email' => $email,
                        'is_logged_in' => true,
                        'userId' => $userId,
                        'firstname' => $firstname,
                        'emp_id' =>$emp_id
                    );

                    $this->session->set_userdata($data);
                     $this->session->set_flashdata('success_login', "Login Successful");
                    if($flag ==1)
                    {
                        redirect('welcome/mydashboard');
                    }else{
                        redirect('welcome/home');
                    }

                }else{
                    $password_error="Employe ID Is Not Matching !!.";
                    $this->session->set_flashdata('pass_msg', $password_error);
                    redirect('welcome/index');
                }
                $this->session->set_userdata($data);
                redirect('welcome/index');
            }
        }
    }

    public function register()
    {
        if($_POST){
            $firstname=$this->input->post('name');
            $empid=$this->input->post('empid');
            $email=$this->input->post('email');
            $emailexist= $this->user->emailexist($email);

            if($emailexist == 0)
            {


            $data=array(
                'empname'=>$firstname,
                'emp_id'=>md5(md5($empid)),
                'email'=>$email
            );

            $insert= $this->user->signup($data);
             $this->session->set_flashdata('register_msg', "register Successful");
            redirect('welcome/ourindex');
            }else{
                $this->session->set_flashdata('msg', 'Email ID Alerady Exist');

                redirect('welcome/index');
            }

        }

    }

    public function approve()
    {
          $reg_id=$this->input->post('reg_id');
         $approve= $this->register->giveapprove($reg_id);


    }

    public function reject()
    {
        $reg_id=$this->input->post('reg_id');
        $approve= $this->register->givereject($reg_id);

    }

    public function deleterow()
    {
       $register_id=$this->input->post('register_id');
       $delete=$this->register->delrow($register_id);


    }


}