<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');
        $this->load->model('register','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->model('feedbacks','',TRUE);
        $this->load->database();
	}

  public function sendmail()
  {
    
      $allemail=$this->user->allemail();
      //$this->mailconfig();
      $register_id=$this->input->post('register_id'); 
      
      foreach($allemail as $email)
      {
        $getuserid = $this->user->getuserid($register_id);
        foreach($getuserid as $userid)
        {

          $user_id=$userid->user_id;
          $getemail = $this->user->getemail($user_id);
          foreach($getemail as $original)
          {

            $mailid=$original->email; 

            if($mailid != $email->email )
            {

                $uid=$email->user_id;
                $message = 'http://localhost/pro/feedback/sendfeedback/?id='.$uid.'&reg='.$register_id;
                $this->email->from('jeevanandam092@gmail.com'); // change it to yours
                $this->email->to($email->email);// change it to yours
                $this->email->subject('Techbytes Feedback');
                $this->email->message($message);
               if($this->email->send())
               {
                echo 'Email sent.';
               }
               else
              {
               show_error($this->email->print_debugger());
              }

            }

          }

        }
                      
      }

  }

  public function sendfeedback()
  {
      $id['id']=$_REQUEST['id'];
      $id['reg_id']=$_REQUEST['reg'];
      $this->load->view('feedback_user',$id);
  }

  public function submitfeedback()
  {

    if($_POST)
    {
       $user_id=$this->input->post('cus_id'); 
       $getname= $this->user->getusername($user_id);
       foreach($getname as $row)
       {
      
       $name = $row->empname;
       }
     
       $reg_id=$this->input->post('reg_id');
       $title=$this->input->post('titles');
       $comments=$this->input->post('comments');

       $data=array('user_id'=>$user_id,
                   'reg_id'=>$reg_id,
                   'title'=>$title,
                   'empname'=>$name,
                   'comments'=>$comments);

       $this->feedbacks->submit($data);
       redirect('welcome/feedback');
    }
  }

  public function viewfeedback()
  {
    if($_POST)
    {
      $reg_id=$this->input->post('register_id');
      $getfeedback['feed'] = $this->feedbacks->getfeedback($reg_id);
       $this->load->view('ajax_view/feedback',$getfeedback);
    }

  }
  
}

