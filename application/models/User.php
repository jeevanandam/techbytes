<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {

    public function emailexist($email)
    {
        $this->db->where('email',$email);
        return $this->db->count_all_results('users');

    }

    public function signup($data)
    {
        $result=$this->db->insert('users',$data);
    }


    public function login($email,$password)
    {

        $this -> db -> select('*');
        $this -> db -> from('users');
        $this -> db -> where('email',$email);
        $this -> db -> where('emp_id',md5(md5($password)));
        $query = $this -> db -> get();
        if($query -> num_rows() == 1){
            return $query->result();
        }else{
            return false;
        }
    }

    public function allemail()
    {
        $this -> db -> select('email,user_id');
        $this -> db -> from('users');
         $query = $this -> db -> get();
         return $query->result();
    }

    public function getuserid($register_id)
    {
        $this -> db -> select('user_id');
        $this -> db -> from('register');
         $this -> db -> where('reg_id',$register_id);
        $query = $this -> db -> get();
        return $query->result();
    }

    public function getemail($user_id)
    {
        $this -> db -> select('email');
        $this -> db -> from('users');
         $this -> db -> where('user_id',$user_id);
        $query = $this -> db -> get();
        return $query->result();
    }

    public function getusername($user_id)
    {
        $this -> db -> select('empname');
        $this -> db -> from('users');
         $this -> db -> where('user_id',$user_id);
        $query = $this -> db -> get();
        return $query->result();
    }
}