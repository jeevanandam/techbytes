<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Model {
	
	public function reg($data)
	{

		$result=$this->db->insert('register',$data);

	 }

	public function getdata()
	{
		$user_id=$this->session->userdata('userId');
		$this -> db -> select('*');
		$this -> db -> from('register');
		$this -> db -> where('user_id',$user_id);
		$query = $this -> db -> get();
		return $query->result();
	}

	public function getunapproved()
	{

		$this -> db -> select('*');
		$this -> db -> from('register reg');
		$this->  db -> join('users user', 'user.user_id=reg.user_id');
		$query = $this -> db -> get();
		return $query->result();
	}

	public function giveapprove($reg_id)
	{

		$this->db->set('approve','approved');
		$this->db->where('reg_id',$reg_id);
		$this->db->update('register');

	}

	public function givereject($reg_id)
	{ 

		$this->db->set('approve','rejected');
		$this->db->where('reg_id',$reg_id);
		$this->db->update('register');

	}


	public function checkdate($date)
	{

		$this ->db-> select('*');
		$this ->db-> from('register');
		$this->db->where('date_time',$date['check']);
		$this->db->where('slot',$date['slot']);
		 return $this->db->count_all_results();

	}

	public function delrow($register_id)
	{
		$this->db->where('reg_id', $register_id);
		$this->db->delete('register');
          
	}






}