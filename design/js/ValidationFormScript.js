$( document ).ready(function() {
    

$('#demo-form2').validate({
    rules:{
	Firstname:
				{
					required:true,
				},
	
	Lastname:
				{
					required:true,
				},
	Email:
				{
				required:true,
				email:true
				},
    Password:
				{
				required:true,
				minlength:3,
				maxlength:10
				},
    password_again:
				{
				equalTo:"#password"
				},
	Company:
				{
					required:true,
				},
	Gender:
				{
					required:true,
				},
	No:
				{
					required:true,
				},
	Street:
				{
					required:true,
				},
	City:
				{
					required:true,
				},
	State:
				{
					required:true,
				},			
	Country:
				{
					required:true,
				},
	Zipcode:
				{
					required:true,
				},
	Contactnumber:
				{
					required:true,
				},
	zip_to:    {
					required:true,
				},
				
          },

          highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }

})
});